namespace :ckeditor do
  def copy_assets(regexp)
    Rails.application.assets.each_logical_path(regexp) do |name, path|
      asset = Rails.root.join('public', 'assets', name)
      p "Copy #{path} to #{asset}"
      FileUtils.mkdir_p(File.dirname(asset))
      FileUtils.cp path, asset
    end
  end

  desc 'Copy ckeditor assets, that cant be used with digest'
  task copy_nondigest_assets: :environment do
    copy_assets /ckeditor\/contents.css/
    copy_assets /ckeditor\/skins\/moono\/.+png/
  end
end

require 'fileutils'

desc "Create nondigest versions of all ckeditor digest assets"
task "assets:precompile" => :environment do
  fingerprint = /\-([0-9a-f]{32})\./
  for file in Dir["public/assets/ckeditor/**/*"]
    # Skip file unless it has a fingerprint
    next unless file =~ fingerprint

    # Get filename of this file without the digest
    # (example) public/assets/ckeditor/config.js
    nondigest = file.sub fingerprint, '.'

    # Create a filename relative to public/assets
    # (example) public/assets/ckeditor/config.js => ckeditor/config.js
    filename = nondigest.sub 'public/assets/', ''
    filename = filename.sub /.gz$/, ''          # Remove .gz for correct asset checking

    # Fetch the latest digest for this file from assets
    latest_digest = Rails.application.assets.find_asset(filename).digest

    # Debug information
    STDOUT.puts '---- ' + file + ' ----'

    # Compare digest of this file to latest digest
    # [1] is the enclosed capture in the fingerprint regex above
    this_digest = file.match(fingerprint)[1]
    if (this_digest == latest_digest)
      # This file's digest matches latest digest, copy
      STDOUT.puts 'Matching digest, copying ' + file
      FileUtils.cp file, nondigest, verbose: true
    else
      # This file's digest doesn't match latest digest, ignore
      STDOUT.puts 'Latest digest: ' + latest_digest
      STDOUT.puts 'This digest:   ' + this_digest
      STDOUT.puts 'Non-matching digest, not copying ' + file
    end

    # Debug information
    STDOUT.puts '---- end ----'
  end
end
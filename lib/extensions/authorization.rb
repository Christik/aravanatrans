module ActiveAdmin
  class BaseController < ::InheritedResources::Base
    module Authorization

      protected

      def rescue_active_admin_access_denied(exception)
        error_message = exception.message

        respond_to do |format|
          
          format.html do
            flash[:error] = error_message
            if current_active_admin_user.role == 'moderator' || current_active_admin_user.role == 'admin'
              redirect_to admin_organizations_path, :alert => exception.message
            else
              redirect_to root_path, :alert => exception.message
            end
            
            
          end

          format.csv { render :text => error_message, :status => :unauthorized}
          format.json { render :json => { :error => error_message }, :status => :unauthorized}
          format.xml { render :xml => "<error>#{error_message}</error>", :status => :unauthorized}
        end
      end
    end
  end
end
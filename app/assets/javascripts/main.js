$(document).ready(function(){

/* ==========================================================================
	Header
	========================================================================== */

	var initStikcyHeader = function() {
		var header = $('.site-header'),
			scrollTop = $(document).scrollTop(),
			limit = 1;

		if (scrollTop >= limit) {
			header.addClass('fixed');
		} else if (scrollTop <= limit) {
			header.removeClass('fixed');
		}
	}

	$(window).ready(function(){
		initStikcyHeader();
	});

	$(window).scroll(function(){
		initStikcyHeader();
	});


/* ========================================================================== */
});
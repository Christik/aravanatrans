class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_filter :configure_permitted_parameters, if: :devise_controller?
  #before_filter :default_titles
  rescue_from ActiveRecord::RecordNotFound, with: :render_404
  
  
  def titles(title, description='', keywords='')
    @page_title       = title
    @page_description = description
    @page_keywords    = keywords
  end
  
  def default_titles
    @page = Text.find_or_create_by(key: params[:controller])
    titles(@page.meta_title, @page.meta_desc, @page.meta_key)
  end
  
  protected
  
  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:name, :avatar, 
        :phone, :site, :add_email, :icq, :skype, :vk, :tw, 
        :email, :password, :password_confirmation) }
    devise_parameter_sanitizer.for(:account_update) { |u| u.permit(:name, :avatar, 
        :phone, :site, :add_email, :icq, :skype, :vk, :tw, 
        :email, :password, :password_confirmation, :current_password) }    
        
  end
  
  private
  
  def render_404
    render text: "404 Not Found", status: 404
  end
end

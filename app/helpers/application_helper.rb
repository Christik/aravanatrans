# encoding: utf-8
module ApplicationHelper
  def display_meta_tags
    render 'shared/meta_tags'
  end
  
  def text_format(text)
    reg = /(<code>[.|\W|\w]+<\/code>)/
    text.split(reg).map{|t| t =~ reg ? t : t.gsub(/\n/, "<br />") }.join()
  end
  
  def nested_cats(categories)
    categories.map do |category, sub_categories|
      render(partial: 'catalog/category', locals:{category:category}) + 
      (sub_categories.length ? content_tag(:ul, nested_cats(sub_categories)) : '')
    end.join.html_safe
  end
  
  def nested_left_cats(categories) 
    categories.map do |category, sub_categories|
      render(partial: 'shared/category', 
        locals:{category:category, sub_categories:sub_categories})
    end.join.html_safe
  end
  
  def tag_link(tag)
    link_to(tag.title, tag_path(tag.id))
  end
  
  def image_preview(object, field)
    resource = active_admin_config.resource_name.to_s.underscore.parameterize('_')
    path = "deleteimg_admin_#{resource}_path"
    if object.send(field+'?')
      image_tag(object.send(field+'_url', :admin))+
      link_to('Удалить', send(path), :class=>'delete_img', 'data-field'=>field)
    end
  end
  
  def rate_class(rate) 
    if rate <= 2
      'red'
    elsif 2 < rate && rate <= 4
      'orange'
    elsif 4 < rate && rate <= 7
      'yellow'
    elsif rate > 7
      'green'
    end
  end
  
  def rate_block(data)
    
    rate = data[:cnt] > 0 ? data[:summ]/data[:cnt]:0
    content_tag(:div, '', 
      :class => rate_class(rate), 
      :style => "width: #{(rate+1)*9.1}%;"
    )
  end
end


# GIT

Копируем проект с репозитория:

  $ git clone git@bitbucket.org:LindoAlex/template.git

# Настройка локального окружения

1) Редактируем файл /config/database.yml, прописываем свои 
настройки соединения с базой данны. Пример:

```ruby
development:
  adapter: mysql2
  encoding: utf8
  database: spz_develop
  pool: 5
  username: root
  password: '13131313'
  host: localhost
test:
  adapter: mysql2
  encoding: utf8
  database: spz_develop
  pool: 5
  username: root
  password: '13131313'
  host: localhost

production:
  adapter: mysql2
  encoding: utf8
  database: spz_develop
  pool: 5
  username: root
  password: '13131313'
  host: localhost
```  
2) Запускаем бандлер:

  $ bundle install

3) Создаем базу данных:

  $ rake db:create

4) Мигрируем таблицы бд:

  $ rake db:migrate

